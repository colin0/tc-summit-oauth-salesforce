public with sharing class BitbucketController {

	private final OAuthConnection conn = new OAuthConnection('Bitbucket');

	public Boolean needsAccess {
		get {
			return this.conn.needsAccess();
		}
	}

	public String userInfo { get; private set; }
	public String userName { get; private set; }
	public String displayName { get; private set; }

	// USER INFO CALLOUT
	public void getUserInfo() {
		if (this.needsAccess) {
			return;
		}

		HttpResponse response = this.conn.getResource('/user');
		parseUserInfoResponse(response);
	}

	private void parseUserInfoResponse(HttpResponse response) {
		Map<String, Object> responseJson = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
		userName = (String)responseJson.get('username');
		displayName = (String)responseJson.get('display_name');
		userInfo = JSON.serializePretty(responseJson);
	}

	public PageReference startOAuth() {
		return this.conn.oauthRedirectPage();
	}

	public PageReference requestAccessToken() {
		String authorizationCode = getRequiredParam('code');
		this.conn.requestAccessToken(authorizationCode);

		// redirect back to original page
		PageReference redirect = Page.BitbucketUserInfo;
		redirect.setRedirect(true);
		return redirect;
	}

	private static String getRequiredParam(String name) {
		String val = ApexPages.currentPage().getParameters().get(name);
		if (String.isBlank(val)) {
			throw new OAuthException('required parameter is missing: ' + name);
		}
		return val;
	}

}
