public with sharing class AccessInfo {

	public Long REFRESH_TOLERANCE_MS = 30000L;

	private final AuthData__c access = AuthData__c.getInstance();

	public Boolean hasAccessToken() {
		return getAccessToken() != null;
	}

	public String getAccessToken() {
		return this.access.AccessToken__c;
	}

	public Boolean hasRefreshToken() {
		return getRefreshToken() != null;
	}

	public String getRefreshToken() {
		return this.access.RefreshToken__c;
	}

	public String getExpiration() {
		return String.valueOf(this.access.Expiration__c);
	}

	public Boolean needsRefresh() {
		if (hasRefreshToken() && this.access.Expiration__c != null) {
			return System.currentTimeMillis() > this.access.Expiration__c.getTime() - REFRESH_TOLERANCE_MS;
		}
		return false;
	}

	public void updateAccess(Map<String, Object> data) {
		this.access.AccessToken__c = (String)data.get('access_token');
		String refreshToken = (String)data.get('refresh_token');

		if (String.isNotBlank(refreshToken)) { // only update refresh token if new value is provided
			this.access.RefreshToken__c = refreshToken;
		}

		Long expiresIn = (Integer)data.get('expires_in') * 1000L;
		this.access.Expiration__c = DateTime.newInstance(System.currentTimeMillis() + expiresIn);
	}

	public void save() {
		upsert this.access;
	}

}
