public with sharing class OAuthConnection {


	private final AccessInfo accessInfo = new AccessInfo();
	private final OAuthConfig__mdt config;

	public OAuthConnection(String configName) {
		this.config = getClientInfo(configName);
	}

	public Boolean needsAccess() {
		return !this.accessInfo.hasAccessToken();
	}

	public PageReference oauthRedirectPage() {
		URLParams queryParams = new URLParams(new Map<String, Object> {
			'response_type' => 'code',
			'client_id' => config.ClientId__c,
			'redirect_uri' => config.RedirectUri__c
		});
		return new PageReference(config.AuthorizationEndpoint__c + '?' + queryParams);
	}

	public HttpResponse send(HttpRequest request) {
		Boolean doRefresh = this.accessInfo.needsRefresh();
		if (doRefresh) {
			refreshAccess();
		}

		// update authorization header AFTER refreshing token
		request.setHeader('Authorization', 'Bearer ' + this.accessInfo.getAccessToken());
		HttpResponse response = new HTTP().send(request);
		checkStatus(response);
		if (doRefresh) {
			this.accessInfo.save(); // save updated token info AFTER calling out
		}
		return response;
	}

	public HttpResponse getResource(String path) {
		return send(makeRequest('GET', path));
	}

	private HttpRequest makeRequest(String method, String path) {
		HttpRequest request = new HttpRequest();
		request.setMethod(method);
		request.setEndpoint(config.WebserviceEndpoint__c + path);
		request.setHeader('Accept', 'application/json');
		return request;
	}

	// ACCESS TOKEN CALLOUT

	public void requestAccessToken(String authorizationCode) {
		HttpRequest request = makeTokenRequest(authorizationCode);
		HttpResponse response = new HTTP().send(request);
		parseTokenResponse(response);
		this.accessInfo.save();
	}

	private HttpRequest makeTokenRequest(String authorizationCode) {
		HttpRequest request = new HttpRequest();
		request.setMethod('POST');
		request.setEndpoint(config.TokenEndpoint__c);
		setTokenRequestHeaders(request);

		URLParams bodyParams = new URLParams(new Map<String, Object> {
			'grant_type' => 'authorization_code',
			'code' => authorizationCode,
			'redirect_uri' => config.RedirectUri__c
		});
		request.setBody(bodyParams.toString());

		return request;
	}

	private void parseTokenResponse(HttpResponse response) {
		checkStatus(response);
		Map<String, Object> responseData = (Map<String, Object>)System.JSON.deserializeUntyped(response.getBody());
		String tokenType = (String)responseData.get('token_type');
		if (tokenType != 'bearer') { // this code only support bearer tokens
			throw new OAuthException('unknown token type: ' + tokenType);
		}

		this.accessInfo.updateAccess(responseData);
	}

	private void setTokenRequestHeaders(HttpRequest request) {
		String basicAuth = computeBasicAuth();
		request.setHeader('Authorization', 'Basic ' + basicAuth);
		request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
		request.setHeader('Accept', 'application/json');
	}

	// REFRESH TOKEN CALLOUT
	private void refreshAccess() {
		HttpRequest request = makeRefreshRequest();
		HttpResponse response = new HTTP().send(request);
		parseTokenResponse(response);
	}

	private HttpRequest makeRefreshRequest() {
		HttpRequest request = new HttpRequest();
		request.setMethod('POST');
		request.setEndpoint(config.TokenEndpoint__c);
		setTokenRequestHeaders(request);
		setRefreshRequestBody(request);
		return request;
	}

	private void setRefreshRequestBody(HttpRequest request) {
		URLParams bodyParams = new URLParams(new Map<String, Object> {
			'grant_type' => 'refresh_token',
			'refresh_token' => this.accessInfo.getRefreshToken()
		});
		request.setBody(bodyParams.toString());
	}

	// HELPERS
	private String computeBasicAuth() {
		String creds = config.ClientId__c + ':' + config.ClientSecret__c;
		return EncodingUtil.base64encode(Blob.valueOf(creds));
	}

	private void checkStatus(HttpResponse response) {
		Integer statusCode;
		if (statusCode < 200 || statusCode > 299) {
			throw new OAuthException('bad response: ' + response);
		}
	}

	private static OAuthConfig__mdt getClientInfo(String developerName) {
		return [
			SELECT ClientId__c, ClientSecret__c, RedirectUri__c,
					AuthorizationEndpoint__c, TokenEndpoint__c, WebserviceEndpoint__c
			FROM OAuthConfig__mdt
			WHERE DeveloperName = :developerName
		];
	}

}
