public with sharing class URLParams {

	private static final String SEPARATOR_CHAR = '&';

	private final List<URLParam> params = new List<URLParam>();

	public URLParams() {}

	public URLParams(Map<String, Object> params) {
		if (params != null) {
			for (String key : params.keySet()) {
				addParam(key, params.get(key));
			}
		}
	}

	public void addParam(String key, Object value) {
		// if our value is a list, we add a key value pair for each element in the list
		if (value instanceof List<Object>) {
			List<Object> l = (List<Object>)value;
			for (Object o : l) {
				addParam(key, o);
			}
		} else {
			this.params.add(new URLParam(key, value));
		}
	}

	public override String toString() {
		if (params.isEmpty())
			return '';
		String sep = '';
		String result = '';
		for (URLParam qp : this.params) {
			if (qp.value != null) {
				result += (sep + qp);
				sep = SEPARATOR_CHAR;
			}
		}
		return result;
	}

	public with sharing class URLParam {
		public final String key;
		public final Object value; 
		public URLParam(String key, Object value) {
			this.key = key;
			this.value = value;
		}

		public override String toString() {
			return urlEncode(this.key) + '=' + urlEncode(String.valueOf(this.value));
		}
	}

	private static final String ENCODING_SCHEME = 'UTF-8';
	private static String urlEncode(String s) {
		return EncodingUtil.urlEncode(s, ENCODING_SCHEME);
	}


}
