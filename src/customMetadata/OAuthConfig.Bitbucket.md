<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Bitbucket</label>
    <protected>true</protected>
    <values>
        <field>AuthorizationEndpoint__c</field>
        <value xsi:type="xsd:string">https://bitbucket.org/site/oauth2/authorize</value>
    </values>
    <values>
        <field>ClientId__c</field>
        <value xsi:type="xsd:string">CVAWTEjfv2DAeR9PtF</value>
    </values>
    <values>
        <field>ClientSecret__c</field>
        <value xsi:type="xsd:string">hCaYb6zReWV99yAmFJdbbvVtw39B68Pe</value>
    </values>
    <values>
        <field>RedirectUri__c</field>
        <value xsi:type="xsd:string">https://oauth-octopus-dev-ed.my.salesforce.com/apex/oauthcallback</value>
    </values>
    <values>
        <field>TokenEndpoint__c</field>
        <value xsi:type="xsd:string">https://bitbucket.org/site/oauth2/access_token</value>
    </values>
    <values>
        <field>WebserviceEndpoint__c</field>
        <value xsi:type="xsd:string">https://api.bitbucket.org/2.0</value>
    </values>
</CustomMetadata>
